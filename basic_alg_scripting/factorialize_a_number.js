/**
 * Created by Ron on 07-Nov-16.
 * Returns x!
 * e.g 2! = 1 * 2 * 3 * 4 * 5
 * = 120
 */

//INPUT num (integer): numeric value to apply operation
//OUTPUT (integer): value of num!
function factorialize(num) {
    let count = 1;// initialize a counter
    let total = count; // assign value of 1

    while (count < num) {
        total *= (count + 1); // multiply and assign total
        count++;
    }

    return total;
}

//TEST #1
if(typeof factorialize(5) == 'number') {
    console.log('Test #1 PASSED: factorialize(5) == \'number\'');
} else {
    console.log('Test #1 FAILED: factorialize(5) = ', typeof factorialize(5), ', should return \'number\'');
}

//TEST #2
if(factorialize(5) == 120) {
    console.log('Test #1 PASSED: factorialize(5) == 120');
} else {
    console.log('Test #1 FAILED: factorialize(5) = ', factorialize(5), ', should return 120');
}

//TEST #3
if(factorialize(10) == 3628800) {
    console.log('Test #1 PASSED: factorialize(10) == 3628800');
} else {
    console.log('Test #1 FAILED: factorialize(10) = ', factorialize(10), 'should return 3628800');
}

//TEST #4
if(factorialize(20) == 2432902008176640000) {
    console.log('Test #1 PASSED: factorialize(20) == 2432902008176640000');
} else {
    console.log('Test #1 FAILED: factorialize(20) = ', factorialize(20), 'should return 2432902008176640000');
}

//TEST #5
if(factorialize(0) == 1) {
    console.log('Test #1 PASSED: factorialize(0) == 1');
} else {
    console.log('Test #1 FAILED: factorialize(0) = ', factorialize(0), 'should return 1');
}