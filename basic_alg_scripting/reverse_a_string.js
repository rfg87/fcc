/**
 * Created by Ron on 07-Nov-16.
 * Returns a reversed string
 */

//INPUT str (string): a string
//OUTPUT (string): str reversed
function reverseString(str) {
    /*split str into array, reverse array, join reversed array into string and return string*/
    return str.split("").reverse().join("");
}// end reverseString

console.log(typeof reverseString("hello"));
console.log(reverseString("hello"));
console.log(reverseString("Howdy"));
console.log(reverseString("Greetings from Earth"));