/**
 * Created by Ron on 20-Nov-16.
 * removes any falsy values in an array
 */

//INPUT arr (array): an array of elements
//OUTPUT (array): array with no falsy values
function bouncer(arr) {
    return arr.filter(Boolean);
}// end bouncer