/**
 * Created by Ron on 20-Nov-16.
 * creates and returns a 2D array of
 * arr's elements grouped in arrays of size arrays
 */

//INPUT arr (array): an array of elements
//INPUT size (integer): number of elements in each group
//OUTPUT (array): 2D array of array of size elements
function chunkArrayInGroups(arr, size) {
    let newArr = [];

    for(let i = 0; i < arr.length; i++) {
        if ((i + size) % size === 0) {// create array to group arr's elements
            newArr.push([]);
        }

        newArr[newArr.length - 1].push(arr[i]);// push element to group
    }

    return newArr;
}// end chunkArrayInGroups

console.log(chunkArrayInGroups(["a", "b", "c", "d"], 2));