/**
 * Created by Ron on 09-Nov-16.
 *
 * returns a sentence with the first character
 * of every word capitalized
 */

//INPUT: str (string): a sentence
//OUTPUT (string): sentence with every word capitalized
function titleCase(str) {
    str = str.split(" ");
    let sentence = "";

    for (let i = 0; i < str.length; i++) {
        for (let j = 0; j < str[i].length; j++) {
            if (j == 0) {
                sentence += str[i][j].toUpperCase();// capitalize first character
            } else {
                sentence += str[i][j].toLowerCase();
            }
        }

        if (i < str.length - 1) {
            sentence += " "; // adds spaces between words
        }
    }

    return sentence;
}