/**
 * Created by Ron on 20-Nov-16.
 * checks if all characters in one string
 * is in another string
 */

//INPUT arr (array): contains 2 strings to be evaluated
//OUTPUT (boolean): returns true if each character in arr[1] exists in arr[0] returns false otherwise
function mutation(arr) {
    /*format both strings*/
    arr[0] = arr[0].toLowerCase();
    arr[1] = arr[1].toLowerCase();

    for (let i = 0; i < arr[1].length; i++) {
        if (arr[0].indexOf(arr[1][i]) === -1) {
            return false;// early exit if arr[1][i] doesn't exist in arr[0]
        }
    }

    return true;
}

console.log(mutation(["hello", "hey"]));