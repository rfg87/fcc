/**
 * Created by Ron on 21-Nov-16.
 * return the index where an integer would
 * be placed in a sorted array
 */

//INPUT arr (array): array of integers
//INPUT num (numeric): number to be inserted in array
//OUTPUT (integer): integer representing where num would be inserted
function getIndexToIns(arr, num) {
    arr.sort(function (a, b) {// sort array in ascending order
        return a - b;
    });

    for (let i = 0; i < arr.length; i++) {// search for index
        if (arr[i] >= num) {
            return i;
        }
    }

    return arr.length;// if num > all the values in arr

}// end getIndexToIns