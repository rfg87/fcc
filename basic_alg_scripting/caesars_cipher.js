/**
 * Created by Ron on 21-Nov-16.
 * decodes a caesar's cipher encoded string
 */
//INPUT str (string): an encoded string
//OUTPUT (string): decoded string
function rot13(str) {
    /*convert str into an arrray
    of array of chracters and iterate
    through each element of character
    array*/
    return str.split('').map.call(str, function(char) {
            let x = char.charCodeAt(0);// convert char into ASCII

            /*shift / formatt according to ASCII value and convert into alphbetic character*/
            if (x < 65 || x > 90) {
                return String.fromCharCode(x);
            }
            else if (x < 78) {
                return String.fromCharCode(x + 13);
            } else {
                return String.fromCharCode(x - 13);
            }
        // convert deciphered valued into string
        }).join('');
}// end rot13
