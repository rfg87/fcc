/**
 * Created by Ron on 11-Nov-16.
 * repeats and appends a string
 * x amount of times
 */

//INPUT str (string): string to be repeated
//INPUT num (integer): amount of times for str to be repeated
//OUTPUT (string): str repeated num times
function repeatStringNumTimes(str, num) {
    let newStr = '';
    for (var i = 0; i < num; i++) {
        newStr += str;
    }

    return newStr;
}

console.log(repeatStringNumTimes("abc", 3));