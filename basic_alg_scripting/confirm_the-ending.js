/**
 * Created by Ron on 11-Nov-16.
 *
 * evaluates whether a string ends with a
 * target string
 */
//INPUT str (string): a sentence
//INPUT target (string): word to be evaluated
//OUTPUT (boolean): evaluation of str ends with target
function confirmEnding(str, target) {
    str = str.split(" ");
    let lastWord = str[str.length - 1];

    if (lastWord.substring(lastWord.length - target.length, lastWord.length) === target) {
        return true;
    } else {
        return false;
    }
}

console.log(confirmEnding("Bastian", "n"));