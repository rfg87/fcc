/**
 * Created by Ron on 07-Nov-16.
 *
 * returns length of longest word in a sentence
 */

//INPUT str (string): string/sentence
//OUTPUT (integer):
function findLongestWord(str) {
    let sentence = str.split(' ');// convert str to an array of strings
    let wordLengths = [];

    for (let i = 0; i < sentence.length; i++) {
        wordLengths.push(sentence[i].length);// push word length integer value to wordLengths array
    }

    /**
     * sort integer values in ascending order
     * return value of last element in array (highest value)
     */
    let lastElement = wordLengths.length - 1;
    let sortedWordLengths = wordLengths.sort(sortNumber);
    return sortedWordLengths[lastElement];
}// end findLongestWord


function sortNumber(a,b) {
    return a - b;
}// end sortNumber

function runTest(sentence, expValue, testNo) {
    if (findLongestWord(sentence) === expValue) {
        console.log("Test #", testNo, " PASSED: findLongestWord('", sentence, "') == ", expValue );
    } else {
        console.log("Test #", testNo, " FAILED: expected ", expValue, " instead got ", findLongestWord(sentence));
    }
}
//TEST #1
if (typeof findLongestWord("The quick brown fox jumped over the lazy dog") == 'number') {
    console.log('Test #1 PASSED: findLongestWord("The quick brown fox jumped over the lazy dog") = number');
} else {
    console.log("Test #1 FAILED: findLongestWord('The quick brown fox jumped over the lazy dog') = ",
        typeof findLongestWord("The quick brown fox jumped over the lazy dog"));
}

let testData = [["The quick brown fox jumped over the lazy dog", 6], ["May the force be with you", 5],
["Google do a barrel roll", 6], ["What is the average airspeed velocity of an unladen swallow", 8],
["What if we try a super-long word such as otorhinolaryngology", 19]];

for (let i = 0; i < testData.length; i++) {
    runTest(testData[i][0], testData[i][1], i + 2);
}