/**
 * Created by Ron on 11-Nov-16.
 *
 * truncates a string (str) according to the following rules:
 * 1. if num is less than or equal to 3 truncate '...' to substring of str[0] - str[num]
 * 2. if num is equal to or greater than str.length return str
 * 3. if str.length + 3 is equal to num return str + '...'
 * 4. if str.length + 3 doesn't equal to num return substring of str[0] to str[n] + '...' where returned str.length
 * equal to num
 *
 */

//INPUT str (string): string to be truncated
//INPUT num (integer): length returned string
//OUTPUT (string): string truncated according to conditions
function truncateString(str, num) {
    let newStr;
    if(num <= 3) {// rule #1
        newStr = str.slice(0, num);
        newStr += "...";
    } else {
        if (num >= str.length) {// rule #2
            newStr = str;
        } else {
            if (str.length + 3 != num) {// rule #4
                newStr = str.slice(0, num - 3);
                newStr += "...";
            } else {// rule #3
                newStr = str;
                newStr += "...";
            }
        }
    }

    return newStr;
}// end truncateString

console.log(truncateString("A-tisket a-tasket A green and yellow basket", "A-tisket a-tasket A green and yellow basket".length));
console.log(truncateString("A-tisket a-tasket A green and yellow basket", "A-tisket a-tasket A green and yellow basket".length + 2))
console.log(truncateString("Peter Piper picked a peck of pickled peppers", 14));