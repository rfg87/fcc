/**
 * Created by Ron on 20-Nov-16.
 * returns an array of n elements
 * removed from the head
 */

//INPUT arr (array): array of n elements
//INPUT howMany (integer): number of elements to remove
//OUTPUT (array): array with howMany integers removed from its head
function slasher(arr, howMany) {
    return arr.slice(howMany);
}// end slasher