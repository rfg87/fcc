/**
 * Created by Ron on 07-Nov-16.
 *
 * def Palindrome: a word or sentence that's spelled the same way both forward and backward,
 * ignoring punctuation, case, and spacing.
 *
 * Determines whether a string is a palindrome.
 */

//INPUT str (string): word to be evaluated
//OUTPUT (boolean): boolean evaluation of str is a palindrome
function palindrome(str) {
    /*evaluate and return boolean of formatted str === formatted and reversed str*/
    return str.toLowerCase().replace(/[^a-z0-9]/gi, '').split('').reverse().join('') === str.toLowerCase().replace(/[^a-z0-9]/gi, '');
}

//TEST #1
if (typeof palindrome("eye") == "boolean") {
    console.log("Test #1 PASSED: palindrome(\"eye\") == boolean");
} else {
    console.log("Test #1 FAILED: palindrome(\"eye\") == ", typeof palindrome("eye"));
}

//TEST #2
if (palindrome("eye")) {
    console.log("Test #2 PASSED: palindrome(\"eye\") == true");
} else {
    console.log("Test #2 FAILED: palindrome(\"eye\") == ",  palindrome("eye"));
}

//TEST #3
if (palindrome("_eye")) {
    console.log("Test #3 PASSED: palindrome(\"_eye\") == true");
} else {
    console.log("Test #3 FAILED: palindrome(\"_eye\") == ",  palindrome("_eye"));
}

//TEST #4
if (palindrome("race car")) {
    console.log("Test #4 PASSED: palindrome(\"race car\") == true");
} else {
    console.log("Test #4 FAILED: palindrome(\"race car\") == ",  palindrome("race car"));
}

//TEST #5
if (!palindrome("not a palindrome")) {
    console.log("Test #5 PASSED: palindrome(\"not a palindrome\") == false");
} else {
    console.log("Test #5 FAILED: palindrome(\"not a palindrome\") == ",  palindrome("not a palindrome"));
}

//TEST #6
if (palindrome("A man, a plan, a canal. Panama")) {
    console.log("Test #6 PASSED: palindrome(\"A man, a plan, a canal. Panama\") == true");
} else {
    console.log("Test #6 FAILED: palindrome(\"A man, a plan, a canal. Panama\") == ",
        palindrome("A man, a plan, a canal. Panama"));
}

//TEST #7
if (palindrome("never odd or even")) {
    console.log("Test #7 PASSED: palindrome(\"never odd or even\") == true");
} else {
    console.log("Test #7 FAILED: palindrome(\"never odd or even\") == ",  palindrome("never odd or even"));
}

//TEST #9
if (!palindrome("nope")) {
    console.log("Test #9 PASSED: palindrome(\"nope\") == false");
} else {
    console.log("Test #9 FAILED: palindrome(\"nope\") == ",  palindrome("nope"));
}

//TEST #10
if (!palindrome("almostomla")) {
    console.log("Test #10 PASSED: palindrome(\"almostomla\") == false");
} else {
    console.log("Test #10 FAILED: palindrome(\"almostomla\") == ",  palindrome("almostomla"));
}

//TEST #11
if (palindrome("My age is 0, 0 si ega ym.")) {
    console.log("Test #11 PASSED: palindrome(\"My age is 0, 0 si ega ym.\") == true");
} else {
    console.log("Test #11 FAILED: palindrome(\"My age is 0, 0 si ega ym.\") == ",  palindrome("My age is 0, 0 si ega ym."));
}

//TEST #12
if (!palindrome("1 eye for of 1 eye.")) {
    console.log("Test #12 PASSED: palindrome(\"1 eye for of 1 eye.\") == false");
} else {
    console.log("Test #12 FAILED: palindrome(\"1 eye for of 1 eye.\") == ",  palindrome("1 eye for of 1 eye."));
}

//TEST #13
if (palindrome("0_0 (: /-\ :) 0-0")) {
    console.log("Test #13 PASSED: palindrome(\"0_0 (: /-\ :) 0-0\") == false");
} else {
    console.log("Test #13 FAILED: palindrome(\"0_0 (: /-\ :) 0-0\") == ",  palindrome("0_0 (: /-\ :) 0-0"));
}

//TEST #14
if (!palindrome("five|\_/|four")) {
    console.log("Test #14 PASSED: palindrome(\"five|\_/|four\") == false");
} else {
    console.log("Test #14 FAILED: palindrome(\"five|\_/|four\") == ",  palindrome("five|\_/|four"));
}
