/**
 * Created by Ron on 09-Nov-16.
 *
 * returns an array of the largest numbers in a 2D array
 */

//INPUT arr (array): 2D array of integers
//OUTPUT (array): array of largest numbers of arr
function largestOfFour(arr) {
    let largestNumbers = [];

    for (let i = 0; i < arr.length; i++) {
        let sorted_arr = arr[i].sort(sortNumber);// sort in ascending values
        largestNumbers.push(sorted_arr[sorted_arr.length - 1]);// assign largest value of arr[i]
    }

    return largestNumbers;
}

function sortNumber(a,b) {
    return a - b;
}

console.log(largestOfFour([[13, 27, 18, 26], [4, 5, 1, 3], [32, 35, 37, 39], [1000, 1001, 857, 1]]));