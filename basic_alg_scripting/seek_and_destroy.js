/**
 * Created by Ron on 21-Nov-16.
 * removes values passed through arguments
 * from an array
 */

//INPUT arr (array): an array
//OUTPUT (array): a filtered array
function destroyer(arr) {
    var args = Array.prototype.slice.call(arguments);// converts passed arguments into an array

    /*remove the array within array to leave only
    * the values to be filtered*/
    args.splice(0, 1);

    return arr.filter(function (element) {
        return args.indexOf(element) === -1;
    });// return filtered array
}// end destroyer

console.log(destroyer([1, 2, 3, 1, 2, 3], 2, 3));